﻿<!DOCTYPE HTML>

<html>

<head>

<meta charset="utf-8">

<meta name="renderer" content="webkit|ie-comp|ie-stand">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />

<meta http-equiv="Cache-Control" content="no-siteapp" />

<title>欢迎登录后台管理系统</title>

    <link rel="icon" href="style/loginSpecial/images/favicon.ico" type="image/x-icon"/>

    <link rel="shortcut icon" href="style/loginSpecial/images/favicon.ico" type="image/x-icon"/>

    <link href="style/loginSpecial/css/default.css" rel="stylesheet" type="text/css" />

    <link href="style/loginSpecial/css/styles.css" rel="stylesheet" type="text/css" />

    <link href="style/loginSpecial/css/demo.css" rel="stylesheet" type="text/css" />

    <link href="style/loginSpecial/css/loaders.css" rel="stylesheet" type="text/css" />

    <script src="style/loginSpecial/js/jquery-2.1.1.min.js"></script>

</head>

<body>



    <div class='page'>



        <div class="page_logo">

                    <div class="MyLogo_text"> <img class="" src="style/loginSpecial/images/logo.png" alt="LOGO">

                    <h1 class="MyLogo_text">来客，让每个企业都拥有自己的电商平台</h1>

        </div>    	

        </div>





        <div class="login">
            <form name="form1" action="index.php?module=Login" class="form form-horizontal" method="post"   enctype="multipart/form-data" >
                <div class="page_login">

                            <div class='login_title'>

                                <span>管理员登录</span>

                            </div>

                            <div class='login_fields'>

                                <div class='login_fields__user'>

                                    <div class='icon'>

                                        <img alt="" src='style/loginSpecial/img/user_icon_copy.png'>

                                    </div>

                                    <input name="login" placeholder='用户名' maxlength="16" class="username" type='text' autocomplete="off"/>

                                    <div class='validation'>  

                                    </div>

                                </div>



                                <div class='login_fields__password'>

                                    <div class='icon'>

                                        <img alt="" src='style/loginSpecial/img/lock_icon_copy.png'>

                                    </div>

                                    <input name="pwd" class="passwordNumder" placeholder='密码' maxlength="16" type='text' autocomplete="off">

                                    <div class='validation'>

                                    </div>

                                </div>

                                <div class='login_fields__submit' >
                                      <button  type="submit" style="border-radius: 50px;
                                        background: transparent;
                                        padding: 10px 50px;
                                        border: 2px solid #4FA1D9;
                                        color: #4FA1D9;
                                        text-transform: uppercase;
                                        font-size: 16px;
                                        -webkit-transition-property: background,color;
                                        transition-property: background,color;
                                        -webkit-transition-duration: .2s;
                                        transition-duration: .2s" name="Submit">登录</button> 
                                    <!-- <input type='button' value='登录'> -->
                                </div>



                            </div>

                            <div class='success'>

                            </div>

                            <div class='disclaimer'>

                                <p>欢迎登陆来客电商平台</p>

                            </div>



                </div>

        </form>

        </div>

    </div>





    <div class='authent'>

        <div class="loader" style="height: 60px;width: 60px;margin-left: 28px;margin-top: 40px">

            <div class="loader-inner ball-clip-rotate-multiple">

                <div></div>

                <div></div>

                <div></div>

            </div>

        </div>

        <p>认证中...</p>

    </div>

    <div class="OverWindows"></div>

<link href="style/loginSpecial/layui/css/layui.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="style/loginSpecial/js/jquery-ui.min.js"></script>

<script type="text/javascript" src='style/loginSpecial/js/stopExecutionOnTimeout.js?t=1'></script>

<script src="style/loginSpecial/layui/layui.js" type="text/javascript"></script>

<script src="style/loginSpecial/js/Particleground.js" type="text/javascript"></script>

<script src="style/loginSpecial/js/Treatment.js" type="text/javascript"></script>

<script src="style/loginSpecial/js/jquery.mockjax.js" type="text/javascript"></script>

<!-- <script src="style/loginSpecial/js/controlLogin.js" type="text/javascript"></script> -->

</body>

</html>