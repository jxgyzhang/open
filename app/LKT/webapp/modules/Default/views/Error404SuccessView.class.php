<?php

/**

 * [Laike System] Copyright (c) 2018 laiketui.com

 * Laike is not a free software, it under the license terms, visited http://www.laiketui.com/ for more details.

 */

class Error404SuccessView extends SmartyView
{

    public function execute ()
    {

        // set our template
        $this->setTemplate('Error404Success.tpl');

    }

}

?>